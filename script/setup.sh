# install mono
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
sudo apt-get update && sudo apt-get -y install apt-transport-https
sudo echo "deb https://download.mono-project.com/repo/ubuntu stable-xenial main" | sudo tee /etc/apt/sources.list.d/mono-official-stable.list
sudo apt-get update && sudo apt-get -y install mono-devel
# need to install cuda 9.0 for use with openmm 7.2.2
wget http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_9.0.176-1_amd64.deb
sudo dpkg -i cuda-repo-ubuntu1604_9.0.176-1_amd64.deb
sudo apt-get update && sudo apt-get install cuda=9.0.176-1
# install conda and openmm, for benchmark comparisons. 
curl https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh > miniconda3.sh 
chmod +x ./miniconda3.sh && ./miniconda3.sh -b
export PATH=~/miniconda/bin:$PATH
conda install -y -c omnia openmm
# set up paths for openmm plugin, add them to current shell and bashrc for any ssh connections.
echo 'export LD_LIBRARY_PATH=~/openmm/lib:$LD_LIBRARY_PATH' >> ~/.bashrc
echo 'export OPENMM_PLUGIN_DIR=~/openmm/lib/plugins' >> ~/.bashrc
export LD_LIBRARY_PATH=~/openmm/lib:$LD_LIBRARY_PATH
export OPENMM_PLUGIN_DIR=~/openmm/lib/plugins
# open up the ports we need:
sudo iptables -I INPUT 1 -p tcp --dport 8080 -j ACCEPT
sudo iptables -I INPUT 1 -p tcp --dport 80 -j ACCEPT
sudo bash -c "iptables-save > /etc/iptables.rules"