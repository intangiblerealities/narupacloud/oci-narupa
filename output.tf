# Output the private and public IPs of the instance
#output "ServerPrivateIPs" {
#  value = ["${oci_core_instance.NarupaServer.*.private_ip}"]
#}

#output "ServerHostnames" {
#  value = ["${oci_core_instance.NarupaServer.*.display_name}"]
#}

output "ServerPublicIPs" {
  value = ["${oci_core_instance.NarupaServer.*.public_ip}"]
}

output "SSHPublicKey" {
  value = "${oci_core_instance.NarupaServer.*.metadata.ssh_authorized_keys}"
}