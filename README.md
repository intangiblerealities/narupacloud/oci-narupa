# README 

A proof-of-principle Terraform configuration for a GPU-enabled Narupa server on Oracle Cloud. 

This terraform configuration demonstrates how to spin up a CUDA-enabled GPU server with Terraform on Oracle Cloud Compute. It uses [terraform-provider-oci](https://github.com/oracle/terraform-provider-oci).

To use it, either rename `terraform.tfvars.example` to `terraform.tfvars` and fill it with your information, or source an environment file, such as that shown in `credentials.example.sh`. Details on where to get all the required keys from are available [here](https://docs.cloud.oracle.com/iaas/Content/API/Concepts/apisigningkey.htm).

Once you've set up all your keys and paths, run the following: 

```
terraform init 
terraform plan
terraform apply
```

It will spit out details of what it's going to do, ask for permission, then output the server public IPs which can be used to SSH into the machine as `ubuntu`. From there you can run the server:

```
ssh ubuntu@PUBLIC_IP
```

```
mono bin/narupa-server.exe
```

Once it's up an running, you can connect from a build of NarupaXR (currently from the develop branch). 
A pre-built binary of a compatible VR client is available at [this link](https://drive.google.com/file/d/1V3hHtHWTC2yyOSLilWo5TmuaK4wKc26g/view?usp=sharing).
Right now, it requires manual configuration of the server details. The file `narupaXR-cloud/NarupaXR_Data/ServerSettings/server.json` should be editted
like so: 

```
{
  "Descriptor": "Direct Connection",
  "Uri": "ws://PUBLIC_IP:8080/",
  "SessionKey": "473GVnWtcqsHNAPGJdw9T4eqTGRd+LY3xKpWpfZ/KCE="
}
```

where PUBLIC_IP is the public IP address used to log into the server. 

## Additional Notes 

You can change the desired compute shape in variables.tf. VMs are pretty cheap, GPUs and bare metal machines are expensive!

This setup is intended for internal use, with an existing Narupa image on Oracle servers. The `setup.tf.bootstrap` file contains the original setup scripts used to initialise a CUDA image from scratch, copying over narupa and openmm. 
A compatible Ubuntu test build of Narupa and OpenMM are available [here](https://drive.google.com/file/d/1kL7W5JpNuDaA_nV100NfI23vBtEoRaX5/view?usp=sharing).
*If you are deploying a fresh build of narupa, be sure to adjust the paths to wherever your build of Narupa and OpenMM are*. 
In the future, container deployment will be used.
