//Configuration of virtual network.
resource "oci_core_virtual_network" "NarupaVCN" {
  cidr_block     = "10.1.0.0/16"
  compartment_id = "${var.compartment_ocid}"
  display_name   = "NarupaVCN"
  dns_label      = "narupavcn"
}

resource "oci_core_subnet" "NarupaSubnet" {
  count               = "${length(var.ADS)}"
  availability_domain = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[var.ADS[count.index] -1 ],"name")}"
  cidr_block          = "10.1.${count.index}.0/24"
  display_name        = "SubnetAD${var.ADS[count.index]}"
  dns_label           = "subnetAD${var.ADS[count.index]}"
  security_list_ids   = ["${oci_core_virtual_network.NarupaVCN.default_security_list_id}", "${oci_core_security_list.NarupaSecurityList.id}"]
  compartment_id      = "${var.compartment_ocid}"
  vcn_id              = "${oci_core_virtual_network.NarupaVCN.id}"
  route_table_id      = "${oci_core_route_table.NarupaRT.id}"
  dhcp_options_id     = "${oci_core_virtual_network.NarupaVCN.default_dhcp_options_id}"
}

resource "oci_core_internet_gateway" "NarupaIG" {
  compartment_id = "${var.compartment_ocid}"
  display_name   = "NarupaIG"
  vcn_id         = "${oci_core_virtual_network.NarupaVCN.id}"
}

resource "oci_core_route_table" "NarupaRT" {
  compartment_id = "${var.compartment_ocid}"
  vcn_id         = "${oci_core_virtual_network.NarupaVCN.id}"
  display_name   = "NarupaRT"

  route_rules {
    destination       = "0.0.0.0/0"
    network_entity_id = "${oci_core_internet_gateway.NarupaIG.id}"
  }
}

resource "oci_core_security_list" "NarupaSecurityList" {
  compartment_id = "${var.compartment_ocid}"
  vcn_id         = "${oci_core_virtual_network.NarupaVCN.id}"
  display_name   = "NarupaSecurityList"

  // allow inbound ssh traffic from a specific port
  ingress_security_rules = [
    {
      # Open all ports within the private network
      protocol = "6"
      source   = "10.0.0.0/8"
    },
    {
      # Open port for Narupa
      protocol = "6"
      source   = "0.0.0.0/0"

      tcp_options {
        min = 8000
        max = 8010
      }
    },
    {
      # Open port for Narupa wss
      protocol = "6"
      source   = "0.0.0.0/0"

      tcp_options {
        min = 80
        max = 80
      }
    },
    {
      # Open port for Narupa wss
      protocol = "6"
      source = "0.0.0.0/0"

      tcp_options {
        min = 8080
        max = 8080
      }
    },
  ]

    // allow outbound tcp traffic
  egress_security_rules = [
    {
      # Open port for Narupa
      protocol = "6"
      destination   = "0.0.0.0/0"

      tcp_options {
        min = 8000
        max = 8010
      }
    },
    {
      # Open port for Narupa wss
      protocol = "6"
      destination = "0.0.0.0/0"

      tcp_options {
        min = 80
        max = 80
      }
    },
    {
      # Open port for Narupa wss
      protocol = "6"
      destination = "0.0.0.0/0"

      tcp_options {
        min = 8080
        max = 8080
      }
    },
  ]
}
