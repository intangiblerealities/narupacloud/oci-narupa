//Sets up the instance.
resource "oci_core_instance" "NarupaServer" {
  availability_domain = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[var.ServerAD - 1], "name")}"
  compartment_id      = "${var.compartment_ocid}"
  display_name        = "narupa"
  shape               = "${var.ServerShape}"

  create_vnic_details {
    # ServerAD
    #subnet_id        = "${oci_core_subnet.NarupaSubnet.id}"
    subnet_id = "${oci_core_subnet.NarupaSubnet.*.id[index(var.ADS, var.ServerAD)]}"

    display_name     = "primaryvnic"
    assign_public_ip = true
    hostname_label   = "narupa"
  }

  source_details {
    source_type = "image"
    source_id   = "${var.ServerImageOCID[var.region]}"
  }

  metadata {
    ssh_authorized_keys = "${var.ssh_public_key}"
  }

  timeouts {
    create = "60m"
  }

  freeform_tags = {
    "narupa"  = "${var.NarupaNameTag}"
    "nodetype" = "narupa"
  }
}