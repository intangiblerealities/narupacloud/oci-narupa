variable "tenancy_ocid" {}
variable "user_ocid" {}
variable "fingerprint" {}
variable "private_key_path" {}
variable "region" {}

variable "compartment_ocid" {}
variable "ssh_public_key" {}
variable "ssh_private_key" {}

variable "ADS" {
  description = "The list of ADs you want to create your narupa across."
  default = ["3"]
}

variable "ServerAD" {
  description = "The AD the management node should live in."
  default = "3"
}

variable "ServerShape" {
  description = "The shape to use for the management node"
  default = "VM.GPU2.1"
}

variable "ServerImageOCID" {
  description = "What image to use for the management node. A map of region name to image OCID."
  type = "map"

  default = {
    // See https://docs.us-phoenix-1.oraclecloud.com/images/
    // Ubuntu 16.04
    uk-london-1    = "ocid1.image.oc1.uk-london-1.aaaaaaaajxg7h2afqlesebr3qde2q562juauobyotat3aiphjikbf4v72z3a"
    // Ubuntu 16.04 GPU image
    //eu-frankfurt-1 = "ocid1.image.oc1.eu-frankfurt-1.aaaaaaaauilrmwndm7qslryagyh5mh6kifdzerakii27bt5kas3s62loe7ma"
    // Our image with everything set up.
    eu-frankfurt-1 = "ocid1.image.oc1.eu-frankfurt-1.aaaaaaaar4s7kf3mncwvrxmrjrbtugdqmk5jtd7yes2fseelbmvohwzf3bwa"
  }
}

variable "BootStrapFile" {
  default = "./script/setup.sh"
}

variable "NarupaNameTag" {
  default = "narupa"
}
